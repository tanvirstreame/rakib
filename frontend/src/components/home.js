import React from "react";
import Header from "./header";
import TeacherList from "./teacherList";

const Home = () => {
    return (
        <div>
            <Header/>
            <TeacherList/>
        </div>
    )
}

export default Home;
