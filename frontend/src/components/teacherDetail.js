import React, { useState, useEffect } from "react";
import fakeData from "../FakeData/fakeData";
import image from "../assets/teacher.jpg";
import { Link } from "react-router-dom";
import Header from "./header";

const TeacherList = () => {

    const [teacher, setTeacher] = useState([]);

    useEffect(() => {
        setTeacher(fakeData);
    }, [])

    return (
        <div>
            <Header />
            <div className="container teacher-detail">
                <div className="row">
                    <div className="col-md-6">
                        <iframe className="w-100 resume-video" src="https://www.youtube.com/embed/d0yGdNEWdn0">
                        </iframe>
                    </div>
                    <div className="col-md-6">

                    </div>
                </div>
            </div>
        </div>
    )
}

export default TeacherList;